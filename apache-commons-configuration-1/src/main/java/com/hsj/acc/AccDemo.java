package com.hsj.acc;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;

import java.util.Iterator;

/**
 * 读取属性文件配置
 */
public class AccDemo {

    public static void main(String[] args) throws ConfigurationException {
        Configuration configuration = new PropertiesConfiguration("1.properties");
        System.out.println(configuration.getString("common.name"));
        System.out.println(configuration.getString("common.fullname"));
        System.out.println(configuration.getInt("common.age"));
        System.out.println(configuration.getString("common.addr"));
        System.out.println(configuration.getLong("common.count"));

        // 打印include的内容
        System.out.println(configuration.getString("java.version"));

        System.out.println();
        System.out.println("=====使用subset方法得到一个子配置类=====");

        Configuration subset = configuration.subset("common");
        Iterator<String> iterator = subset.getKeys();
        while (iterator.hasNext()){
            String k = iterator.next();
            System.out.println(k +"--->" +subset.getString(k));
        }

        System.out.println("系统属性===========");
        Configuration systemConfig = new SystemConfiguration();
        Iterator<String> keys = systemConfig.getKeys();
        while (keys.hasNext()){
            System.out.println(keys +"-->"+ keys.next());
        }



    }

}
