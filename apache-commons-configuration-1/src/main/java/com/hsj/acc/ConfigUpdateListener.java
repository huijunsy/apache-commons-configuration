package com.hsj.acc;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConfigurationUtils;
import org.apache.commons.configuration.PropertiesConfiguration;


/**
 * 配置更新监听
 */
public class ConfigUpdateListener {

    public static void main(String[] args) throws ConfigurationException {
        PropertiesConfiguration con = new PropertiesConfiguration("1.properties");

        con.addConfigurationListener(event->{
            Object source = event.getSource();
            int type = event.getType();
            if (event.isBeforeUpdate()){
                System.out.println("事件源："+source.getClass());
                System.out.println("事件类型"+ type);
            }
        });

        con.addProperty("hsj","hahha");
        ConfigurationUtils.dump(con,System.out);


    }

}
