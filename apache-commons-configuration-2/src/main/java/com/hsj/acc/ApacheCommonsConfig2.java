package com.hsj.acc;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.event.ConfigurationEvent;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;

/**
 * 2.2.7版本示例
 */
public class ApacheCommonsConfig2 {

    public static void main(String[] args) throws ConfigurationException {

        Configurations configurations = new Configurations();

        PropertiesConfiguration pcf = configurations.properties("1.properties");

        pcf.addEventListener(ConfigurationEvent.ADD_PROPERTY,event->{
            if (!event.isBeforeUpdate()){
                System.out.printf("成功添加属性：%s = %s", event.getPropertyName(), event.getPropertyValue());
            }
        });

        pcf.addProperty("hjc","hahah");

    }



}
